package com.estebanmarin
package scalac

import zio.test._
import zio.test.Assertion._

object ExampleSpec extends DefaultRunnableSpec {

  def spec: Spec[Any, TestFailure[Nothing], TestSuccess] = suite("ExampleSpec")(test("addition works") {
    assert(1 + 1)(equalTo(2))
  })

}
