package com.estebanmarin.scalac.integration.github

import zio._
import zhttp.http.Header
import zhttp.service.Client
import com.estebanmarin.scalac.domain.Contributor
import com.estebanmarin.scalac.domain.Organization
import com.estebanmarin.scalac.config.Config.ServerConfig
import com.estebanmarin.scalac.error.GHClientError
import com.estebanmarin.scalac.config.Config
import zhttp.http.UHttpResponse
import zhttp.service.{ ChannelFactory, EventLoopGroup }

// Module pattern
//Interface definition
trait GHClient {
  def getAllOrgs: ZIO[Has[ServerConfig], GHClientError, List[Organization]]
  def getContributorsByOrg(org: String): ZIO[Has[ServerConfig], GHClientError, List[Contributor]]
  def getString: UIO[String]
}

object GHClient {
  private val baseURL = "https://api.github.com/"
  private val org     = "organizations"
  private val headers = List(Header.acceptAll)

  //Interface Implementation
  // Has[ServerConfig] to add to this closure the GHTKEN env variable, to be used in the
  // Client call
  val live: ZLayer[Has[ServerConfig], GHClientError, Has[GHClient]] =
    ZLayer.succeed {
      new GHClient {
        override def getAllOrgs: ZIO[Has[ServerConfig], GHClientError, List[Organization]]                       = ???
        // Missing call to Client.request, there is missing wiring to enable the Client call.
        override def getContributorsByOrg(org: String): ZIO[Has[ServerConfig], GHClientError, List[Contributor]] = ???
        // Missing call to Client.request, there is missing wiring to enable the Client call.
        override def getString: UIO[String] =
          //Wiring test with http server app
          ZIO.succeed("Hello")

      }
    }

  private def request(
    url: String,
    headers: List[Header]
  ): ZIO[EventLoopGroup with ChannelFactory, Throwable, UHttpResponse] =
    // review https://github.com/dream11/zio-http/blob/release%2Fnext/example/src/main/scala/example/HttpsClient.scala
    Client.request(url, headers)

  // accesors
  def getString: URIO[Has[GHClient], String] =
    ZIO.serviceWith[GHClient](_.getString)

}
