package com.estebanmarin.scalac.config

import zio.config._, ConfigDescriptor._

object Config {
  final case class ServerConfig(port: Int, ghtoken: String)

  val serverConfig: ConfigDescriptor[ServerConfig] =
    (int("PORT") |@| string("GHTOKEN"))(ServerConfig.apply, ServerConfig.unapply)

  val live = ZConfig.fromSystemEnv(serverConfig)
}
