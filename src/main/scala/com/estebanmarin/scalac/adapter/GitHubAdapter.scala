package com.estebanmarin.scalac.adapter

import zio._
import zio.console._
import com.estebanmarin.scalac.domain.Organization
import org.http4s.blaze.http.HttpClient
import com.estebanmarin.scalac.http.HttpDecodingClient
import org.http4s.client.Client
import org.http4s.client.blaze.BlazeClientBuilder

trait GitHubAdapter {
  def getOrganizations: IO[Throwable, List[Organization]]
}

object GitHubAdapter {
  def getOrganizations: ZIO[Has[GitHubAdapter], Throwable, List[Organization]] =
    ZIO.serviceWith[GitHubAdapter](_.getOrganizations)
}

case class GitHubAdapterLive(httpClient: HttpClient) extends GitHubAdapter {

  def getOrganizationsGithub(http4sClient: TaskManaged[Client[Task]]): RIO[ZEnv, List[Organization]] = {
    val layer             = http4sClient.toLayer.orDie
    val http4sClientLayer = (layer) >>> HttpDecodingClient.http4s
    val program           = for {
      result: List[Organization] <- Github.getOrganizations
      _                          <- putStrLn(result.toString())
    } yield result

    program.provideSomeLayer[ZEnv](http4sClientLayer)
  }

  def makeHttpDecodingClient: UIO[TaskManaged[Client[Task]]]       =
    ???

  override def getOrganizations: IO[Throwable, List[Organization]] = ???
  // for {
  //   client <- makeHttpDecodingClient
  //   result <- getOrganizationsGithub(client)
  // } yield result

}
