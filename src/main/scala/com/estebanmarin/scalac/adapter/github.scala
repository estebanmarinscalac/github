package com.estebanmarin.scalac.adapter

import zio._
import io.circe.generic.auto._
import com.estebanmarin.scalac.domain.Organization
import com.estebanmarin.scalac.http.HttpDecodingClient.{ get, HttpClient }
import com.estebanmarin.scalac.domain.Contributor
import com.estebanmarin.scalac.error.DecodingError

object Github {

  def getOrganizations: RIO[HttpClient, List[Organization]] =
    get[List[Organization]]("organizations")
      .tapError(_ => ZIO.succeed(DecodingError("Error decoding Organization API")))

  def getContributorsByOrg(organization: String): RIO[HttpClient, List[Contributor]] = {
    val url = s"${organization}"
    get[List[Contributor]](url)
      .tapError(_ => ZIO.succeed(DecodingError("Error decoding Contributors API")))
  }

}
