package com.estebanmarin
package scalac.http.server

import zio._

import com.estebanmarin.scalac.domain.Organization
import io.circe.generic.auto._
import io.circe.syntax._
import zhttp.http.Method.GET
import zhttp.http._
import zhttp.service._

object ZIOHttpServer {

  // fedding client to the app so its able to query the gitHub app
  // once the server is instantiated
  def httpRoutes(response: List[Organization]): HttpApp[Any, Nothing] = HttpApp.collect {
    case GET -> Root / "org"        => Response.jsonString(response.asJson.noSpaces)
    case GET -> Root / "org" / name => Response.jsonString(response.asJson.noSpaces)
    // case GET -> Root / "org" / name => Response.jsonString(response.asJson.noSpaces)
  }

  // Layer creation for dependency injection
  val live: ZLayer[Any with Any, Nothing, ChannelFactory with EventLoopGroup] =
    ChannelFactory.auto ++ EventLoopGroup.auto()

  //server singlenton
  def server(port: Int, httpRoutes: HttpApp[Any, Nothing]): ZIO[ZEnv, Throwable, Nothing] =
    Server.start(port, httpRoutes)

}
