package com.estebanmarin.scalac.http

import zio._

import org.http4s.client.Client
import io.circe.Decoder
import org.http4s.Http

object HttpDecodingClient {

  type HttpClient  = Has[Service]
  type Response[T] = RIO[HttpClient, T]

  trait Service {
    def get[T](uri: String)(implicit d: Decoder[T]): Task[T]
  }

  def get[T](uri: String)(implicit d: Decoder[T]): RIO[HttpClient, T] =
    RIO.accessM[HttpClient](_.get.get[T](uri))

  def http4s: URLayer[Has[Client[Task]], HttpClient] =
    ZLayer.fromService[Client[Task], Service] { http4sClient =>
      Http4s(http4sClient)
    }

}
