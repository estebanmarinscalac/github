package com.estebanmarin.scalac.http

import zio._
import zio.interop.catz._

import io.circe.Decoder
import org.http4s.circe.CirceEntityCodec.circeEntityDecoder
import org.http4s.client.Client
import org.http4s.client.dsl.Http4sClientDsl
import org.http4s.implicits._

final case class Http4s(client: Client[Task]) extends HttpDecodingClient.Service with Http4sClientDsl[Task] {

  def get[T](uri: String)(implicit d: Decoder[T]): Task[T] = {
    val target = uri"https://api.github.com/" / uri
    client
      .expect[T](target.toString())
      .foldM(
        e => IO.fail(e),
        ZIO.succeed(_)
      )
  }

}
