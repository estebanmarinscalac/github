package com.estebanmarin.scalac.domain

final case class Organization(
  login: String,
  id: Int,
  node_id: String,
  url: String,
  repos_url: String,
  events_url: String,
  hooks_url: String,
  issues_url: String,
  members_url: String,
  public_members_url: String,
  avatar_url: String,
  description: Option[String]
)
