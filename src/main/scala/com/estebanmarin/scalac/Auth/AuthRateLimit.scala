package com.estebanmarin.scalac.Auth

import zio._

import java.time.Instant
import pdi.jwt.{ JwtCirce, JwtAlgorithm, JwtClaim }
import com.estebanmarin.scalac.config.Config
import com.estebanmarin.scalac.error.JwtTokenError

trait AuthRateLimit {
  def getAuthToken: ZIO[Has[Config.ServerConfig], Throwable, String]
}

object AuthRateLimit {

  private val claim: IO[Throwable, JwtClaim] = ZIO.effect(
    JwtClaim(
      expiration = Some(Instant.now.plusSeconds(157784760).getEpochSecond),
      issuedAt = Some(Instant.now.getEpochSecond)
    )
  )

  // key: String = "secretKey"
  val algo = JwtAlgorithm.HS256

  private def generateToken(claim: JwtClaim, gthToken: String): IO[Throwable, String] = ZIO.effect(
    JwtCirce.encode(claim, gthToken, algo)
  )

  val getString: ULayer[Has[AuthRateLimit]] =
    ZLayer.succeed {
      new AuthRateLimit {
        override def getAuthToken = (for {
          jwtClaim     <- claim
          serverConfig <- ZIO.environment[Has[Config.ServerConfig]]
          token        <- generateToken(jwtClaim, serverConfig.get.ghtoken)
        } yield token)
          .tapError(e => ZIO.fail(JwtTokenError("Error creating JWT token")))
      }
    }

  def getAuthToken: ZIO[Has[Config.ServerConfig] with Has[AuthRateLimit], Throwable, String] = ???
  // ZIO.serviceWith[AuthRateLimit](_.getAuthToken)
}
