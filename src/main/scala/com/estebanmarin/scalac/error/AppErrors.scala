package com.estebanmarin.scalac.error

// single file to abstract and enclose all errors caused by the application
// this is done to make the error handling consistent
abstract class ScalaCBaseError(message: String) extends Throwable(message)

case class GHClientError(message: String) extends ScalaCBaseError(message)

case class DecodingError(message: String) extends ScalaCBaseError(message)

case class JwtTokenError(message: String) extends ScalaCBaseError(message)

case class ImproveErrorManagement(message: String) extends ScalaCBaseError(message)
