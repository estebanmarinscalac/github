package com.estebanmarin
package scalac

import zio._
import zio.console._
import zio.interop.catz._
import zio.magic.ZioProvideMagicOps

import com.estebanmarin.scalac.adapter._
import com.estebanmarin.scalac.config.Config
import com.estebanmarin.scalac.domain.Organization
import com.estebanmarin.scalac.error.{ ImproveErrorManagement, ScalaCBaseError }
import com.estebanmarin.scalac.http.HttpDecodingClient
import com.estebanmarin.scalac.http.server.ZIOHttpServer

import org.http4s._
import org.http4s.client.Client
import org.http4s.client.blaze.BlazeClientBuilder
import org.http4s.dsl.Http4sDsl
import org.http4s.implicits._

object Main extends App {
  def run(args: List[String]): URIO[ZEnv, ExitCode] =
    myApp.exitCode

  def makeHttpClient: UIO[TaskManaged[Client[Task]]] =
    ZIO.runtime[Any].map { implicit rts =>
      val exec = rts.platform.executor.asEC
      BlazeClientBuilder[Task](exec).resource.toManaged
    }

  // SERVICE
  def gitHubCall(http4sClient: TaskManaged[Client[Task]]): RIO[ZEnv, List[Organization]] = {
    val layer             = http4sClient.toLayer.orDie
    val http4sClientLayer = (layer) >>> HttpDecodingClient.http4s
    val program           = for {
      result: List[Organization] <- Github.getOrganizations
      _                          <- putStrLn(result.toString())
    } yield result

    program.provideSomeLayer[ZEnv](http4sClientLayer)
  }

  val myApp: IO[ScalaCBaseError, Unit] =
    (for {
      _                                       <- putStrLn("-" * 50)
      // Fetching env viariables
      serverConfig: Has[Config.ServerConfig]  <- ZIO.environment[Has[Config.ServerConfig]]
      http4sClient: TaskManaged[Client[Task]] <- makeHttpClient
      organizations: List[Organization]       <- gitHubCall(http4sClient)
      app: zhttp.http.HttpApp[Any, Nothing]    = ZIOHttpServer.httpRoutes(organizations)
      _                                       <- ZIOHttpServer.server(serverConfig.get.port, app)
      _                                       <- putStrLn("-" * 50)
    } yield ())
      // using ZIO magic to abstract layering ZIO v2.0 default
      .inject(
        //ZIO console ...etc
        ZEnv.live,
        // Config layering for env variables
        Config.live
        // Integration adapter
        // ZIOHttpServer.live
        // GHClient.live
      )
      .catchAll((error: Throwable) =>
        // This is a massage to the developer to improve error handling in case
        // as well to log any issue with the app
        IO.fail(ImproveErrorManagement(s"Improve error handling!!! ${error.getMessage()}"))
      )

}
