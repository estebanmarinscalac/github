import sbt._

object Dependencies {

  case object com {

    case object github {

      case object alexarchambault {
        val `scalacheck-shapeless_1.15` =
          "com.github.alexarchambault" %% "scalacheck-shapeless_1.15" % "1.3.0"
      }

      case object zio             {
        val zio_v1      = "dev.zio"              %% "zio"              % "1.0.12"
        val zio_test    = "dev.zio"              %% "zio-test"         % "1.0.12" % Test
        val zio_magic   = "io.github.kitlangton" %% "zio-magic"        % "0.3.10"
        val zio_http    = "io.d11"               %% "zhttp"            % "1.0.0.0-RC17"
        val zio_config  = "dev.zio"              %% "zio-config"       % "1.0.10"
        val zio_interop = "dev.zio"              %% "zio-interop-cats" % "2.5.1.0"
      }

      case object cats            {
        val catsEffectVersion = "3.2.9"
        val catsEffect        = "org.typelevel" %% "cats-effect" % catsEffectVersion
      }

      case object http4s          {
        val http4sVersion = "0.21.3"
        val http4sDsl     = "org.http4s" %% "http4s-dsl"          % http4sVersion
        val blazeServer   = "org.http4s" %% "http4s-blaze-server" % http4sVersion
        val blazeClient   = "org.http4s" %% "http4s-blaze-client" % http4sVersion
        val httpCirce     = "org.http4s" %% "http4s-circe"        % http4sVersion
      }

      case object circe           {
        val circeVersion = "0.14.1"
        val circeCore    = "io.circe" %% "circe-core"    % circeVersion
        val circeGeneric = "io.circe" %% "circe-generic" % circeVersion
        val circeParser  = "io.circe" %% "circe-parser"  % circeVersion
      }

      case object jwtcirce        {
        val version       = "9.0.2"
        val jwtcircescala = "com.github.jwt-scala" %% "jwt-circe" % version
      }

      case object liancheng       {
        val `organize-imports` =
          "com.github.liancheng" %% "organize-imports" % "0.5.0"
      }

    }

    case object olegpy {
      val `better-monadic-for` =
        "com.olegpy" %% "better-monadic-for" % "0.3.1"
    }

  }

  case object org {

    case object augustjune    {
      val `context-applied` =
        "org.augustjune" %% "context-applied" % "0.1.4"
    }

    case object scalacheck    {
      val scalacheck =
        "org.scalacheck" %% "scalacheck" % "1.15.4"
    }

    case object scalatest     {
      val scalatest =
        "org.scalatest" %% "scalatest" % "3.2.10"
    }

    case object scalatestplus {
      val `scalacheck-1-15` =
        "org.scalatestplus" %% "scalacheck-1-15" % "3.2.10.0"
    }

    case object typelevel     {
      val `discipline-scalatest` =
        "org.typelevel" %% "discipline-scalatest" % "2.1.5"

      val `kind-projector` =
        "org.typelevel" %% "kind-projector" % "0.13.2" cross CrossVersion.full
    }

  }

}
