import Dependencies._

ThisBuild / organization := "com.estebanmarin"
ThisBuild / scalaVersion := "2.13.7"

enablePlugins(JavaAppPackaging)

lazy val `scalac`            =
  project
    .in(file("."))
    .settings(name := "scalac")
    .settings(commonSettings)
    .settings(dependencies)

lazy val commonSettings      =
  compilerPlugins ++ commonScalacOptions ++ Seq(
    update / evictionWarningOptions := EvictionWarningOptions.empty
  )

lazy val compilerPlugins     = Seq(
  addCompilerPlugin(com.olegpy.`better-monadic-for`),
  addCompilerPlugin(org.augustjune.`context-applied`),
  addCompilerPlugin(org.typelevel.`kind-projector`)
)

lazy val commonScalacOptions = Seq(
  Compile / console / scalacOptions := {
    (Compile / console / scalacOptions).value
      .filterNot(_.contains("wartremover"))
      .filterNot(Scalac.Lint.toSet)
      .filterNot(Scalac.FatalWarnings.toSet) :+ "-Wconf:any:silent"
  },
  Test / console / scalacOptions    :=
    (Compile / console / scalacOptions).value
)

lazy val dependencies        = Seq(
  libraryDependencies ++= Seq(
    // main dependencies
    com.github.zio.zio_v1,
    com.github.zio.zio_test,
    com.github.zio.zio_magic,
    com.github.zio.zio_http,
    com.github.zio.zio_config,
    com.github.zio.zio_interop,
    com.github.http4s.blazeClient,
    com.github.http4s.blazeServer,
    com.github.http4s.http4sDsl,
    com.github.circe.circeCore,
    com.github.circe.circeGeneric,
    com.github.circe.circeParser,
    com.github.http4s.httpCirce,
    com.github.jwtcirce.jwtcircescala
    // com.github.cats.catsEffect
  ),
  libraryDependencies ++= Seq(
    com.github.alexarchambault.`scalacheck-shapeless_1.15`,
    org.scalacheck.scalacheck,
    org.scalatest.scalatest,
    org.scalatestplus.`scalacheck-1-15`,
    org.typelevel.`discipline-scalatest`
  ).map(_ % Test)
)
