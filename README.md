# ESTEBAN MARIN SCALAC

Thanks for opening the opportunity to apply for Scalac 🙏.

Here is a [short video walkthough of the code](https://www.youtube.com/watch?v=gmcmj1Sa5ow)

## About the project

This is a Scala backend with hexagonal architecture that:

- Has opinionated SBT settings that provide a better developer experience.
- Leverage in the ZIO ecosystem
- `Has[error-handling]`
- Use ZIO magic for dependency injection and better layering (ZIO v2.0 default)
- `zhhtp server`, performant by default
- `zio-config` for env variables setup

Feel free to navigate, most of the code is commented

## What needs to be improved

As with all code challenges, there is a constant compromise, there will always be things to improve.
This codebase is no exception, here I list all the issues, in order of importance:

- Wire GHClient with HTTP service
- Layer GHCClient to make petitions within the service
- ADD unit tests
- Create a native image and remove JVM for production deployment
- ..... hope we can discuss these and many other

## Before starting, make sure to add the env variables

**VARIABLES**
Put in a the same shell session

```shell
export PORT=8080 && export GHTOKEN="YOURTOKENHERE"
```

otherwise you will find an error like such when running the app

```shell
╥
╠══╦══╗
║  ║  ║
║  ║  ╠─MissingValue
║  ║  ║ path: GHTOKEN
║  ║  ║ Details: value of type string
║  ║  ▼
║  ║
║  ╠─MissingValue
║  ║ path: PORT
║  ║ Details: value of type int
║  ▼
▼
```

## Running the app

```shell
export PORT=8080 && export GHTOKEN="YOURTOKENHERE"
```
then
```shell
sbt r 
```

## Native image 
Run
```shell
sbt> stage
sbt> docker:publishLocal
```
Run in a new terminal
```shell
docker run --env PORT=8080 --env GHTOKEN="YOURTOKENHERE" --expose 8080 scalac:0.1.0-SNAPSHOT
```
or the native bin
```shell
./target/universal/stage/bin/scalac
```
